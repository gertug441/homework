public class Salary implements  MailObject<Integer>{
    private String company;
    private String to;
    private Integer salary;

    public String getCompany(){ return company; }
    public String getTo(){ return to; }
    public Integer getContent(){ return salary; }

    public Salary(String company, String to, Integer salary){
        this.company = company;
        this.to = to;
        this.salary = salary;
    }

}
