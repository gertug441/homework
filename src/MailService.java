import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class MailService<T> implements Consumer<MailObject<T>>{
    private Map<String, List<T>> mailBox = new MyMap<>();
    public Map<String, List<T>>  getMailBox(){ return mailBox; }

    @Override
    public void accept(MailObject<T> tMailObject) {
        var to = tMailObject.getTo();
        var content = tMailObject.getContent();
        if (!mailBox.containsKey(to)){
            mailBox.put(to, new ArrayList<T>());
        }
        var contents = mailBox.get(to);
        contents.add(content);
        mailBox.put(to, contents);
    }
}
