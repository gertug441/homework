public class MailMessage implements  MailObject<String> {
    private String to;
    private String from;
    private String content;
    public String getTo() { return to; }
    public String getFrom() { return from; }
    public String getContent() { return content; }

    public MailMessage(String from, String to, String message){
        this.to = to;
        this.from = from;
        this.content = message;
    }


}
