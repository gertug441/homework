public interface MailObject<T> {
    String getTo();
    T getContent();
}
